<?php

it('has routes if is not installed', function () {

    $this->get('/installer/step-1')->assertStatus(200);
    $this->get('/installer/step-2')->assertStatus(200);
    $this->get('/installer/step-3')->assertStatus(200);
    $this->get('/installer/step-4')->assertStatus(200);
    $this->get('/installer/step-5')->assertStatus(200);
    $this->get('/installer/step-6')->assertStatus(200);

    $this->get('/')->assertRedirect('/installer/step-1');
});
