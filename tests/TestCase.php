<?php

namespace Dterumal\LaravelInstaller\Tests;

use Dterumal\LaravelInstaller\Traits\InstallApplication;
use Illuminate\Database\Eloquent\Factories\Factory;
use Orchestra\Testbench\TestCase as Orchestra;
use Dterumal\LaravelInstaller\LaravelInstallerServiceProvider;

class TestCase extends Orchestra
{
    /**
     * The callbacks that should be run before the application is destroyed.
     *
     * @var array
     */
    protected $beforeApplicationDestroyedCallbacks = [
        [InstallApplication::class, 'cleanInstallation']
    ];

    /**
     * Boot the testing helper traits.
     *
     * @return array
     */
    protected function setUpTraits()
    {
        $uses = parent::setUpTraits();
        if (isset($uses[\Tests\InstallApplication::class])) {
            $this->installApplication();
        }

        return $uses;
    }

    protected function setUp(): void
    {
        parent::setUp();

        Factory::guessFactoryNamesUsing(
            fn (string $modelName) => 'Dterumal\\LaravelInstaller\\Database\\Factories\\'.class_basename($modelName).'Factory'
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelInstallerServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        config()->set('database.default', 'testing');
    }
}
