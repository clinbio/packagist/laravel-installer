<?php

use Dterumal\LaravelInstaller\DbRequirementsChecker;
use Dterumal\LaravelInstaller\PhpRequirementsChecker;

it('can check php version', function () {
    expect(PhpRequirementsChecker::checkVersion())->toBeBool();
});

it('can check php extensions', function () {
    expect(PhpRequirementsChecker::checkExtensions())->toBeBool();
});

it('can check if app is not installed', function () {
    if(file_exists(storage_path('app/.installed'))) {
        unlink(storage_path('app/.installed'));
    }
    expect(is_app_installed())->toBeFalse();
});

it('can check if app is installed', function () {
    file_put_contents(storage_path('app/.installed'), '');
    expect(is_app_installed())->toBeTrue();
});

it('can check DB connection', function () {
    expect(DbRequirementsChecker::checkConnection())->toBeBool();
});
