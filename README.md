<p align="center"><img src="https://banners.beyondco.de/Laravel%20Installer.png?theme=light&packageManager=composer+require&packageName=dterumal%2Flaravel-installer&pattern=architect&style=style_1&description=A+web+installer+for+your+Laravel+application&md=1&showWatermark=1&fontSize=100px&images=fast-forward"></p>

## Introduction

Laravel Installer provides an easy way to add an installation wizard to your application. It will guide the user through multiple steps to ensure that everything is properly working and finally, it will create a first user.

## Installation

You can install the package via composer:

```bash
composer require dterumal/laravel-installer
```

You can publish all the required files with:

```bash
php artisan laravel-installer:install
```

You can publish the config file with:
```bash
php artisan vendor:publish --tag="laravel-installer_without_prefix-config"
```

Optionally, you can publish the assets using

```bash
php artisan laravel-installer:publish
```

This is the contents of the published config file:

```php
return [
    /*
    |--------------------------------------------------------------------------
    | Server Requirements
    |--------------------------------------------------------------------------
    |
    | This is the default Laravel server requirements, you can add as many
    | as your application require, we check if the extension is enabled
    | by looping through the array and run "extension_loaded" on it.
    |
    */
    'version' => '7.4.0',
    'extensions' => [
        'bcmath',
        'ctype',
        'fileinfo',
        'json',
        'mbstring',
        'openssl',
        'pdo',
        'tokenizer',
        'xml',
    ],

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Those are the URLs that we will show and use during the installation.
    | Help URL should a support link. If not provided, we won't show it.
    | Finally Login URL is the final callback URL.
    |
    */

    'help_url' => null,

    'login_url' => '/login'
];
```

## Usage

The Laravel Installer will guide the user through multiple steps to make sure that the server is properly configured, the database is available and finally, it will create a first user.

When the installation process is completed, a `.installed` file will be created inside `storage/app`. The presence of the file will act as the only check for the package to know whether the app is installed or not. 

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [dterumal](https://gitlab.sib.swiss/dterumal)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
