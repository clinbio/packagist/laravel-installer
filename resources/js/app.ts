import Vue from 'vue'
import vuetify from './vuetify';
import axios from "axios";
import Routes from './routes'
import VueRouter from "vue-router";
import LaravelInstaller from "@/LaravelInstaller.vue";

let token: any = document.head.querySelector('meta[name="csrf-token"]');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

Vue.use(VueRouter);

Vue.prototype.$http = axios.create({
    timeout: 5000
});

declare global {
    interface Window {
        LaravelInstaller: any
    }
}

window.LaravelInstaller.basePath = '/' + window.LaravelInstaller.path;

let routerBasePath = window.LaravelInstaller.basePath + '/';

if (window.LaravelInstaller.path === '' || window.LaravelInstaller.path === '/') {
    routerBasePath = '/';
    window.LaravelInstaller.basePath = '';
}

const router = new VueRouter({
    routes: Routes,
    mode: 'history',
    base: routerBasePath,
});

const el: HTMLElement = document.getElementById('laravel-installer') || document.createElement('laravel-installer')

new Vue({
    router,
    vuetify,
    render: h => h(LaravelInstaller)
}).$mount(el)
