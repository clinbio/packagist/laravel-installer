import {RouteConfig} from "vue-router";
import Step1 from "@/Pages/Step1.vue";
import Step2 from "@/Pages/Step2.vue";
import Step3 from "@/Pages/Step3.vue";
import Step4 from "@/Pages/Step4.vue";
import Step5 from "@/Pages/Step5.vue";
import Step6 from "@/Pages/Step6.vue";
import DatabaseError from "@/Pages/DatabaseError.vue";


const routes: RouteConfig[] = [
    {
        path: '/',
        redirect: '/step-1'
    },

    {
        path: '/step-1',
        name: 'step-1',
        component: Step1
    },

    {
        path: '/step-2',
        name: 'step-2',
        component: Step2
    },

    {
        path: '/step-3',
        name: 'step-3',
        component: Step3
    },

    {
        path: '/step-4',
        name: 'step-4',
        component: Step4
    },

    {
        path: '/step-5',
        name: 'step-5',
        component: Step5
    },

    {
        path: '/step-6',
        name: 'step-6',
        component: Step6
    },

    {
        path: '/database-error',
        name: 'database-error',
        component: DatabaseError
    },
]

export default routes
