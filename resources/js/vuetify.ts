import Vue from 'vue'
import Vuetify, { UserVuetifyPreset } from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import en from 'vuetify/src/locale/en'

Vue.use(Vuetify)

const opts: Partial<UserVuetifyPreset> = {
    lang: {
        locales: { en },
        current: 'en'
    }
}

Vuetify.config.silent = true

export default new Vuetify(opts)
