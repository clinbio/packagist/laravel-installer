<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>LaravelInstaller {{ config('app.name') ? ' - ' . config('app.name') : '' }}</title>

    <title>{{ Config::get('app.name') }}</title>
</head>
<body>
<div id="laravel-installer" v-cloak></div>

<!-- Global LaravelInstaller Object -->
<script>
    window.LaravelInstaller = @json($laravelInstallerScriptVariables);
</script>

<script src="{{asset(mix('app.js', 'vendor/laravel-installer'))}}"></script>
</body>
</html>
