<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')->name('laravel-installer.')->group(function () {
    Route::get('/php-version', 'PhpRequirementController@phpVersion')->name('php-version');
    Route::get('/php-extension', 'PhpRequirementController@phpRequirements')->name('php-extension');
    Route::get('/php-check', 'PhpRequirementController@check')->name('php-check');
    Route::post('/database', 'DatabaseController@store')->name('database');
    Route::post('/migrate', 'DatabaseController@migrate')->name('migrate');
    Route::post('/install', 'InstallationController@install')->name('install');
});

Route::get('/redirect-to-login', 'InstallationController@redirectToLogin')->name('redirect-to-login');

// Catch-all Route...
Route::get('/{view?}', 'HomeController@index')->where('view', '(.*)')->name('laravel-installer.index');

