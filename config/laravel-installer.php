<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Server Requirements
    |--------------------------------------------------------------------------
    |
    | This is the default Laravel server requirements, you can add as many
    | as your application require, we check if the extension is enabled
    | by looping through the array and run "extension_loaded" on it.
    |
    */
    'version' => '7.4.0',
    'extensions' => [
        'bcmath',
        'ctype',
        'fileinfo',
        'json',
        'mbstring',
        'openssl',
        'pdo',
        'tokenizer',
        'xml',
    ],

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Those are the URLs that we will show and use during the installation.
    | Help URL should a support link. If not provided, we won't show it.
    | Finally Login URL is the final callback URL.
    |
    */

    'help_url' => null,

    'login_url' => '/login'
];
