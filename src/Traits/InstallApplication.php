<?php

namespace Dterumal\LaravelInstaller\Traits;

trait InstallApplication
{
    /**
     * Define hook to install the application before each test.
     *
     * @return void
     */
    public function installApplication()
    {
        file_put_contents(storage_path('app/.installed'), '');
    }

    /**
     * Define hook to clean after each test.
     *
     * @return void
     */
    public static function cleanInstallation()
    {
        if (file_exists(storage_path('app/.installed'))) {
            unlink(storage_path('app/.installed'));
        }
    }
}
