<?php

namespace Dterumal\LaravelInstaller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use RuntimeException;

class LaravelInstaller
{
    /**
     * Determine if the given request can access the LaravelInstaller dashboard.
     *
     * @param  Request  $request
     * @return bool
     */
    public static function check(Request $request): bool
    {
        return is_app_installed();
    }

    /**
     * Get the default JavaScript variables for LaravelInstaller.
     *
     * @return array
     */
    public static function scriptVariables(): array
    {
        return [
            'path' => 'installer',
            'appName' => config('app.name'),
            'loginUrl' => url(config('laravel-installer.login_url')),
            'assetsAreCurrent' => self::assetsAreCurrent(),
            'isDownForMaintenance' => App::isDownForMaintenance(),
            'help' => config('laravel-installer.help_url'),
            'defaultDbValues' => [
                'db_database' => env('DB_DATABASE', 'database'),
                'db_username' => env('DB_USERNAME', 'username'),
                'db_password' => env('DB_PASSWORD', 'password'),
                'db_host' => env('DB_HOST', 'localhost'),
            ]
        ];
    }

    /**
     * Determine if LaravelInstaller's published assets are up-to-date.
     *
     * @return bool
     *
     * @throws \RuntimeException
     */
    public static function assetsAreCurrent(): bool
    {
        $publishedPath = public_path('vendor/laravel-installer/mix-manifest.json');

        if (!File::exists($publishedPath)) {
            throw new RuntimeException('LaravelInstaller assets are not published. Please run: php artisan laravel-installer:publish');
        }

        return File::get($publishedPath) === File::get(__DIR__.'/../public/mix-manifest.json');
    }


}
