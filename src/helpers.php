<?php

if (!function_exists('is_app_installed')) {
    function is_app_installed(): bool
    {
        return file_exists(storage_path('app/.installed'));
    }
}

