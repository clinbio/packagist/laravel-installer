<?php

namespace Dterumal\LaravelInstaller;

class PhpRequirementsChecker
{
    /**
     * Check PHP version
     *
     * @return bool
     */
    public static function checkVersion(): bool
    {
        return version_compare(PHP_VERSION, config('laravel-installer.php'), '>=');
    }

    /**
     * Check PHP extensions
     *
     * @return bool
     */
    public static function checkExtensions(): bool
    {
        $requirements = config('laravel-installer.extensions');

        return !collect($requirements)->contains(function ($value) {
            return !extension_loaded($value);
        });
    }
}
