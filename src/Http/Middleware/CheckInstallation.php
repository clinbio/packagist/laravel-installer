<?php

namespace Dterumal\LaravelInstaller\Http\Middleware;

use Closure;

class CheckInstallation
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|null
     */
    public function handle($request, Closure $next)
    {
        if (!is_app_installed() && !$request->is('installer/*')) {
            return redirect()->route('laravel-installer.index', ['view' => 'step-1']);
        }
        return $next($request);
    }

}
