<?php

namespace Dterumal\LaravelInstaller\Http\Controllers;

use Dterumal\LaravelInstaller\Http\Middleware\CheckInstallation;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(CheckInstallation::class);
    }

}

