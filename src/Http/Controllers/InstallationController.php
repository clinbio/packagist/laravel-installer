<?php

namespace Dterumal\LaravelInstaller\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\LaravelInstaller\CreateFirstUser;

class InstallationController
{
    public function install(Request $request)
    {
        (new CreateFirstUser)->create($request->all());

        return response()->json(true);
    }

    public function redirectToLogin(Request $request)
    {
        file_put_contents(storage_path('app/.installed'), '');

        return redirect()->to(config('laravel-installer.login_url'));
    }
}
