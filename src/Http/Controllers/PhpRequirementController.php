<?php

namespace Dterumal\LaravelInstaller\Http\Controllers;

use Dterumal\LaravelInstaller\PhpRequirementsChecker;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PhpRequirementController extends Controller
{
    public function phpRequirements(Request $request): JsonResponse
    {
        $requirements = config('laravel-installer.extensions');

        $extensions = collect($requirements)->map(function ($value) {
            return [
                'name' => $value,
                'status' => extension_loaded($value)
            ];
        });

        return response()->json([
            'extensions' => $extensions
        ]);
    }

    public function phpVersion(Request $request): JsonResponse
    {
        $result = collect([
            'required' => config('laravel-installer.version'),
            'current' => PHP_VERSION,
            'compatible' => version_compare(PHP_VERSION, config('laravel-installer.php'), '>=')
        ]);

        return response()->json($result);
    }

    public function check(Request $request)
    {
        if (PhpRequirementsChecker::checkVersion() && PhpRequirementsChecker::checkExtensions()) {
            return response()->json(true);
        }

        return response()->json(false);
    }
}
