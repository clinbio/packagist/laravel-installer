<?php

namespace Dterumal\LaravelInstaller\Http\Controllers;

use Dterumal\LaravelInstaller\LaravelInstaller;

class HomeController extends Controller
{
    /**
     * Single page application catch-all route.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('laravel-installer::layout', [
            'laravelInstallerScriptVariables' => LaravelInstaller::scriptVariables(),
        ]);
    }
}
