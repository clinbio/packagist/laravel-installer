<?php

namespace Dterumal\LaravelInstaller\Http\Controllers;

use App\Models\User;
use Dterumal\LaravelInstaller\DbRequirementsChecker;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class DatabaseController extends Controller
{
    public function store(Request $request)
    {
        $filtered = $request->only(
            'db_database',
            'db_username',
            'db_password',
            'db_host'
        );

        collect($filtered)->each(function ($value, $key) {
            $this->writeNewEnvironmentFileWith(strtoupper($key), $value);
        });

        $dbConnection = config('database.default');

        config()->set("database.connections.{$dbConnection}.host", $filtered['db_host']);
        config()->set("database.connections.{$dbConnection}.database", $filtered['db_database']);
        config()->set("database.connections.{$dbConnection}.username", $filtered['db_username']);
        config()->set("database.connections.{$dbConnection}.password", $filtered['db_password']);

        DB::purge();

        if (DbRequirementsChecker::checkConnection()) {
            return response()->json('success');
        }

        return response()->json('error', 401);
    }

    /**
     * Write a new environment file with the given key.
     *
     * @param  string  $key
     * @param  string  $value
     * @return void
     */
    protected function writeNewEnvironmentFileWith(string $key, string $value)
    {
        file_put_contents(app()->environmentFilePath(), preg_replace(
            $this->keyReplacementPattern($key),
            $key.'='.$value,
            file_get_contents(app()->environmentFilePath())
        ));
    }

    /**
     * Get a regex pattern that will match env APP_KEY with any random key.
     *
     * @return string
     */
    protected function keyReplacementPattern($key)
    {
        return "/^{$key}=[\w\d]*/m";
    }

    public function migrate(Request $request)
    {
        try {
            Artisan::call('migrate:fresh --seed');
        } catch (Exception $e) {
            ray($e);
            return response()->json('error', 401);
        }

        return response()->json('success');
    }
}
