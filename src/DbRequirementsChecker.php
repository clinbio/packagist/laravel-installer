<?php

namespace Dterumal\LaravelInstaller;

use Exception;
use Illuminate\Support\Facades\DB;

class DbRequirementsChecker
{
    /**
     * Check DB connection
     *
     * @return bool
     */
    public static function checkConnection(): bool
    {
        try {
            DB::connection()->getPdo();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
