<?php

namespace Dterumal\LaravelInstaller;

use Dterumal\LaravelInstaller\Http\Middleware\CheckInstallation;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class LaravelInstallerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
        $this->registerResources();
        $this->defineAssetPublishing();
        $this->offerPublishing();
        $this->registerCommands();
        if (!is_app_installed()) {
            $this->registerRoutes();
        }

        $kernel->pushMiddleware(CheckInstallation::class);
    }

    /**
     * Register the LaravelInstaller routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix' => 'installer',
            'namespace' => 'Dterumal\LaravelInstaller\Http\Controllers',
            'middleware' => 'web',
        ], function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });
    }

    /**
     * Register the LaravelInstaller resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-installer');
    }

    /**
     * Define the asset publishing configuration.
     *
     * @return void
     */
    public function defineAssetPublishing()
    {
        $this->publishes([
            LARAVEL_INSTALLER_PATH.'/public' => public_path('vendor/laravel-installer'),
        ], 'laravel-installer-assets');
    }

    /**
     * Setup the resource publishing groups for LaravelInstaller.
     *
     * @return void
     */
    protected function offerPublishing()
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/../config/laravel-installer.php' => config_path('laravel-installer.php'),
            ], 'laravel-installer-config');
        }
    }

    /**
     * Register the LaravelCluster Artisan commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\PublishCommand::class,
                Console\InstallCommand::class,
            ]);
        }
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!defined('LARAVEL_INSTALLER_PATH')) {
            define('LARAVEL_INSTALLER_PATH', dirname(__DIR__).'/');
        }

        $this->configure();
    }

    /**
     * Setup the configuration for LaravelInstaller.
     *
     * @return void
     */
    protected function configure()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/laravel-installer.php', 'laravel-installer'
        );
    }

}
