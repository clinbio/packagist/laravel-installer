<?php

namespace Dterumal\LaravelInstaller\Console;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-installer:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish all of the LaravelInstaller resources';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', [
            '--tag' => 'laravel-installer-assets',
            '--force' => true,
        ]);
    }

}
