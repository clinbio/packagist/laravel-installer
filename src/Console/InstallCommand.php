<?php

namespace Dterumal\LaravelInstaller\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-installer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Laravel-Installer actions and resources';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // Publish...
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-installer-assets', '--force' => true]);
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-installer-config', '--force' => true]);

        // Directories...
        (new Filesystem)->ensureDirectoryExists(app_path('Actions/LaravelInstaller'));

        // Actions...
        copy(__DIR__.'/../../stubs/app/Actions/LaravelInstaller/CreateFirstUser.php', app_path('Actions/LaravelInstaller/CreateFirstUser.php'));
    }
}
